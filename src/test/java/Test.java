import com.cnsugar.ai.face.FaceHelper;
import com.cnsugar.ai.face.SeetafaceBuilder;
import com.cnsugar.ai.face.bean.Result;
import com.seetaface2.model.SeetaRect;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class Test {

    private void init() {
        SeetafaceBuilder.build();//系统启动时先调用初始化方法

        //等待初始化完成
        while (SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.LOADING || SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.READY) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @org.junit.Test
    public void testCompare() throws Exception {
        init();

        String img1 = "D:\\face\\yg.png";
        String img2 = "D:\\face\\gtl.png";
        System.out.println("result:" + FaceHelper.compare(new File(img1), new File(img2)));
    }

    @org.junit.Test
    public void testRegister() throws IOException {
        //将F:\ai\star目录下的jpg、png图片都注册到人脸库中，以文件名为key
        Collection<File> files = FileUtils.listFiles(new File("D:\\face"), new String[]{"jpg", "png"}, false);
        for (File file : files) {
            String key = file.getName();
            try {
                FaceHelper.register(key, FileUtils.readFileToByteArray(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println(1);
    }

    @org.junit.Test
    public void testSearch() throws IOException {
        init();

        long l = System.currentTimeMillis();
        Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File("D:\\face\\zwz.png")));
        System.out.println("搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
    }

    @org.junit.Test
    public void testDetect() throws IOException {
        SeetaRect[] rects = FaceHelper.detect(FileUtils.readFileToByteArray(new File("D:\\face\\刘诗诗-bbbbbbbbbbbbbbbbbb.jpg")));
        if (rects != null) {
            for (SeetaRect rect : rects) {
                System.out.println("x="+rect.x+", y="+rect.y+", width="+rect.width+", height="+rect.height);
            }
        }
    }

    @org.junit.Test
    public void testCorp() throws IOException {
        BufferedImage image = FaceHelper.crop(FileUtils.readFileToByteArray(new File("D:\\face\\gtl.png")));
        if (image != null) {
            ImageIO.write(image, "jpg", new File("D:\\face\\gtl-corp.jpg"));
        }
    }

    @org.junit.Test
    public void testDelete() {
        FaceHelper.removeRegister("zwz.png");
    }

    @org.junit.Test
    public void testJVM() throws Exception {
        init();
        testRegister();
        for (int i = 0; i < 100000; i++) {
            long l = System.currentTimeMillis();
            Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File("D:\\face\\yg.png")));
            System.out.println(i+"-搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
        }
    }
}
